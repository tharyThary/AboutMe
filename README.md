## About me

### I am Thary. 
          I am GNU/Linux-user and fervid partisan of the Free Software. I think
          proprietary software is hidden threat. I believe and hope that someday
          free and open software wins on proprietary software. And we, GNU/Linux
          and BSD users, should bring this day. The day when everyone in the
          Internet will be free and when shackles of evil corporation will be
          broken! I very like be anonymous. I think that anonymous on the
          network is necessary, because without it, evil corporations will trade
          your data.

### Contact me:
<a href="https://matrix.to/#/@thary:inex.rocks"><img src="https://gitlab.com/tharyThary/AboutMe/-/raw/main/matrix_logo.png" title="My Matrix" height="50" width="50" /></a> &nbsp;
<a href='mailto:thary@tuta.io'><img src="https://gitlab.com/tharyThary/AboutMe/-/raw/main/email.png" title="My E-Mail" height="50" width="50" /></a> &nbsp;
<a href='https://conversations.im/i/thary@creep.im'><img src="https://gitlab.com/tharyThary/AboutMe/-/raw/main/xmpp.png" title="My XMPP" height="50" width="50" /></a> &nbsp;
<a href='https://t.me/Thary_thary'><img src="https://gitlab.com/tharyThary/AboutMe/-/raw/main/tg-logo.jpg" title="My Telegram" height="50" width="50" /></a> &nbsp;
<!--[![Flutter](https://img.shields.io/matrix/twim.matrix.org)](https://t.me/Thary_thary)-->


---
[![Flutter](https://img.shields.io/badge/-My_Telegram_Channel-edf1f4?style=for-the-badge&logo=telegram&logoColor=3776ab)](https://t.me/Thary_thary)
[![Flutter](https://img.shields.io/badge/-My_Telegram_Chat-edf1f4?style=for-the-badge&logo=telegram&logoColor=3776ab)](https://t.me/Thary_thary)
[![Flutter](https://img.shields.io/badge/-Chat_Rules-edf1f4?style=for-the-badge&logo=telegram&logoColor=3776ab)](https://t.me/Thary_thary)
<!--[My telegram channel](https://t.me/tharyLinux). My telegram channel is about Linux and free software (open-source). I lead channel on Russian language.
I has [chat](https://t.me/TharyLinuxChat). But read [rules](https://t.me/tharyLinux/472) before going.-->
[![Flutter](https://img.shields.io/badge/-My_Mastodon-edf1f4?style=for-the-badge&logo=mastodon&logoColor=3776ab)](https://mastodon.ml/@thary)
[![Flutter](https://img.shields.io/badge/-My_Matrix_Space-edf1f4?style=for-the-badge&logo=matrix&logoColor=3776ab)](https://matrix.to/#/%23s-linux-project:inex.rocks)
---
